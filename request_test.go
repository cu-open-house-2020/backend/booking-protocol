package message

import (
	"testing"

	"github.com/stretchr/testify/assert"
	pconstant "gitlab.com/cu-open-house-2020/backend/booking-protocol/constant"
)

func TestSymmetricOnRequest(t *testing.T) {
	sender := &BookingRequest{
		VisitorID:   "3da012c5-d33d-4558-a640-d29c18be5610",
		TimeslotID:  435,
		EventID:     123,
		Type:        pconstant.MessageType.Request.Reserve,
		PartitionID: 123456,
	}

	receiver := &BookingRequest{}
	receiver.Deserialize(sender.Serialize())

	assert.Equal(
		t,
		sender,
		receiver,
		"Should be symmetric on sender, receiver",
	)
}

func TestSerializeRequest(t *testing.T) {
	request := &BookingRequest{
		VisitorID:   "214ff164-5a6b-42fd-bec3-5b9b4a5adb2d",
		TimeslotID:  904,
		EventID:     112,
		Type:        pconstant.MessageType.Request.Reserve,
		PartitionID: 123456,
	}
	assert.Equal(
		t,
		[]byte("214ff164-5a6b-42fd-bec3-5b9b4a5adb2d.904.112.1.123456"),
		request.Serialize(),
		"Should serialize correctly [1]",
	)

	request2 := &BookingRequest{
		VisitorID:   "603101.7521",
		TimeslotID:  99999,
		EventID:     99999,
		Type:        pconstant.MessageType.Request.Cancel,
		PartitionID: 123456,
	}
	assert.Equal(
		t,
		[]byte("603101.7521.99999.99999.0.123456"),
		request2.Serialize(),
		"Should serialize correctly [2]",
	)
}

func TestDeserializeRequest(t *testing.T) {
	request := &BookingRequest{}
	request.Deserialize([]byte("682144f4-d535-4042-a272-d01439be749b.435.150.1.123456"))
	assert.Equal(
		t,
		&BookingRequest{
			VisitorID:   "682144f4-d535-4042-a272-d01439be749b",
			TimeslotID:  435,
			EventID:     150,
			Type:        pconstant.MessageType.Request.Reserve,
			PartitionID: 123456,
		},
		request,
		"Should deserialize correctly [1]",
	)

	assert.Error(
		t,
		request.Deserialize([]byte("682144f4-d535-4042-a272-d01439be749b.43x.150.1")),
		"Should error when deserialize abnormal data [1]",
	)

	assert.Error(
		t,
		request.Deserialize([]byte("682144f4-d535-4042-a272-d01439be749b.43.1x0.1")),
		"Should error when deserialize abnormal data [2]",
	)

	assert.Error(
		t,
		request.Deserialize([]byte("682144f4-d535-4042-a272-d01439be749b.43.150.c")),
		"Should error when deserialize abnormal data [3]",
	)

	assert.Error(
		t,
		request.Deserialize([]byte("682144f4-d535-4042-a272-d01439be749b.43.150.1.28e")),
		"Should error when deserialize abnormal data [4]",
	)

	assert.Error(
		t,
		request.Deserialize([]byte("682144f4-d535-4042-a272-d01439be749b.43")),
		"Should error when deserialize abnormal data [5]",
	)
}

func BenchmarkSerializeRequest(b *testing.B) {
	request := &BookingRequest{
		VisitorID:   "214ff164-5a6b-42fd-bec3",
		TimeslotID:  71176,
		EventID:     71236,
		Type:        pconstant.MessageType.Request.Reserve,
		PartitionID: 123456,
	}

	// run function b.N times
	for n := 0; n < b.N; n++ {
		request.Serialize()
	}
}

func BenchmarkDeserializeRequest(b *testing.B) {
	request := &BookingRequest{}

	// run function b.N times
	for n := 0; n < b.N; n++ {
		request.Deserialize([]byte("682144f4-d535-4042-a272-d01439be749b.435.150.1.123456"))
	}
}
