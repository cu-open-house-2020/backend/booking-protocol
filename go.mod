module gitlab.com/cu-open-house-2020/backend/booking-protocol

go 1.13

require (
	github.com/stretchr/testify v1.4.0
	golang.org/x/tools v0.0.0-20200103221440-774c71fcf114 // indirect
)
