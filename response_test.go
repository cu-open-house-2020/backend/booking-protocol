package message

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	pconstant "gitlab.com/cu-open-house-2020/backend/booking-protocol/constant"
)

func TestSymmetricOnResponse(t *testing.T) {
	sender := &BookingResponse{
		CorrespondRequest: &BookingRequest{
			VisitorID:   "3da012c5-d33d-4558-a640-d29c18be5610",
			TimeslotID:  4358782,
			EventID:     123984535,
			Type:        1,
			PartitionID: 123456,
		},
		StatusCode: http.StatusConflict,
	}

	receiver := &BookingResponse{}
	receiver.Deserialize(sender.Serialize())

	assert.Equal(
		t,
		sender,
		receiver,
		"Should be symmetric on sender, receiver",
	)

}

func TestSerializeResponse(t *testing.T) {
	response := &BookingResponse{
		CorrespondRequest: &BookingRequest{
			VisitorID:   "6e7fbf19-df46-41d0-b1d5-cbe34b4605de",
			TimeslotID:  904,
			EventID:     112,
			Type:        pconstant.MessageType.Request.Reserve,
			PartitionID: 123456,
		},
		StatusCode: http.StatusOK,
	}
	assert.Equal(
		t,
		append([]byte("200."), response.CorrespondRequest.Serialize()...),
		response.Serialize(),
		"Should serialize correctly [1]",
	)

}

func TestDeserializeResponse(t *testing.T) {
	correspondRequest := &BookingRequest{
		VisitorID:   "1234",
		TimeslotID:  904,
		EventID:     112,
		Type:        pconstant.MessageType.Request.Reserve,
		PartitionID: 123456,
	}

	response := &BookingResponse{}
	response.Deserialize(append([]byte("417."), correspondRequest.Serialize()...))

	assert.Equal(
		t,
		&BookingResponse{
			CorrespondRequest: correspondRequest,
			StatusCode:        http.StatusExpectationFailed,
		},
		response,
		"Should deserialize correctly [1]",
	)

	assert.Error(
		t,
		response.Deserialize(append([]byte("4x."), correspondRequest.Serialize()...)),
		"Should error when deserialize abnormal data [1]",
	)

	assert.Error(
		t,
		response.Deserialize(correspondRequest.Serialize()),
		"Should error when deserialize abnormal data [2]",
	)
}

func BenchmarkSerializeResponse(b *testing.B) {
	response := &BookingResponse{
		CorrespondRequest: &BookingRequest{
			VisitorID:   "6e7fbf19-df46-41d0-b1d5-cbe34b4605de",
			TimeslotID:  904,
			EventID:     112,
			Type:        pconstant.MessageType.Request.Reserve,
			PartitionID: 123456,
		},
		StatusCode: http.StatusOK,
	}

	// run function b.N times
	for n := 0; n < b.N; n++ {
		response.Serialize()
	}
}

func BenchmarkDeserializeResponse(b *testing.B) {
	request := &BookingResponse{}

	// run function b.N times
	for n := 0; n < b.N; n++ {
		request.Deserialize([]byte("418.682144f4-d535-4042-a272-d01439be749b.435.150.1.123456"))
	}
}
