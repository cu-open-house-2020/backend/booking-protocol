package utils

// DigitsCount : count number of digit in number
func DigitsCount(number int) int {
	if number == 0 {
		return 1
	}
	count := 0
	for number != 0 {
		number /= 10
		count++
	}
	return count

}

// Sum : sum of array
func Sum(input []int) int {
	sum := 0
	for _, val := range input {
		sum += val
	}
	return sum
}
