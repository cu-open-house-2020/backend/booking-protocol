package pconstant

type request struct {
	Reserve int
	Cancel  int
}

type response map[int]string

type messageType struct {
	Request  request
	Response response
}

// MessageType : enum for message type
var MessageType messageType = messageType{
	Request: request{
		Cancel:  0,
		Reserve: 1,
	},
}
