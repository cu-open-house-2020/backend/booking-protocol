package message

import (
	"bytes"
	"fmt"
	"strconv"

	"gitlab.com/cu-open-house-2020/backend/booking-protocol/utils"
)

// BookingResponse : response of booking request
type BookingResponse struct {
	CorrespondRequest *BookingRequest
	StatusCode        int
}

// Serialize BookingResponse to []byte
func (res *BookingResponse) Serialize() []byte {
	serializedRequest := res.CorrespondRequest.Serialize()
	data := make(
		[]byte,
		len(serializedRequest)+utils.DigitsCount(res.StatusCode)+1,
	)

	index := 0
	copy(data, []byte(strconv.Itoa(res.StatusCode)))
	index += utils.DigitsCount(res.StatusCode)

	data[index] = dot
	index++

	copy(data[index:], serializedRequest)

	return data
}

// Deserialize from []byte to BookingResponse
func (res *BookingResponse) Deserialize(data []byte) (err error) {
	extracted := bytes.SplitN(data, []byte{dot}, 2)
	if len(extracted) != 2 {
		err = fmt.Errorf("Expected 2 sections, but found %d", len(extracted))
		return
	}

	res.StatusCode, err = strconv.Atoi(string(extracted[0]))
	if err != nil {
		return
	}

	res.CorrespondRequest = &BookingRequest{}

	return res.CorrespondRequest.Deserialize(extracted[1])
}
