package message

import (
	"bytes"
	"fmt"
	"strconv"

	"gitlab.com/cu-open-house-2020/backend/booking-protocol/utils"
)

// BookingRequest : request for booking at specific timeslot
type BookingRequest struct {
	VisitorID   string
	TimeslotID  int
	EventID     int
	Type        int // Use enum
	PartitionID int
}

const dot = byte('.')

// Serialize BookingRequest to []byte
func (req *BookingRequest) Serialize() []byte {
	lens := []int{
		len(req.VisitorID),
		utils.DigitsCount(req.TimeslotID),
		utils.DigitsCount(req.EventID),
		utils.DigitsCount(req.Type),
		utils.DigitsCount(req.PartitionID),
	}

	data := make(
		[]byte,
		utils.Sum(lens)+4, // 4 = number of dots
	)
	index := 0
	copy(data, []byte(req.VisitorID))
	index += lens[0]

	data[index] = dot
	index++

	copy(data[index:], []byte(strconv.Itoa(req.TimeslotID)))
	index += lens[1]

	data[index] = dot
	index++

	copy(data[index:], []byte(strconv.Itoa(req.EventID)))
	index += lens[2]

	data[index] = dot
	index++

	copy(data[index:], []byte(strconv.Itoa(req.Type)))
	index += lens[3]

	data[index] = dot
	index++

	copy(data[index:], []byte(strconv.Itoa(req.PartitionID)))

	return data
}

// Deserialize from []byte to BookingRequest
func (req *BookingRequest) Deserialize(data []byte) (err error) {
	extracted := bytes.SplitN(data, []byte{dot}, 5)
	if len(extracted) != 5 {
		err = fmt.Errorf("Expected 5 sections, but found %d", len(extracted))
		return
	}

	req.VisitorID = string(extracted[0])
	req.TimeslotID, err = strconv.Atoi(string(extracted[1]))
	if err != nil {
		return
	}
	req.EventID, err = strconv.Atoi(string(extracted[2]))
	if err != nil {
		return
	}
	req.Type, err = strconv.Atoi(string(extracted[3]))
	if err != nil {
		return
	}
	req.PartitionID, err = strconv.Atoi(string(extracted[4]))
	if err != nil {
		return
	}

	return
}
